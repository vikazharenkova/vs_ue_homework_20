﻿// VS_Homework_20.1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

// ПЕРЕЧИСЛЕНИЯ
enum class Result
{
    SUCCESS,
    FAIL,
    ERROR_WRITE,
    ERROR_READ
};

enum class Another
{
    WORK,
    SUCCESS
};

Result DoWork()
{
    return Result::ERROR_WRITE;
}

enum Alphabet
{
    A,
    B,
    C
};


// СТРУКТУРЫ
struct Bag
{
    std::string books[];
};

struct Student
{
    int Age = 0;
    int Height = 0;
    std::string Name = 0;
    Bag* myBag = nullptr;
     
    void GetInfo()
    {
        std::cout << "student struct";
    }
};

int main()
{
    // ПЕРЕЧИСЛЕНИЯ
    Result workResult = DoWork();
    std::cout << static_cast<int>(workResult) << std::endl;
    static_cast<Result>(3);
    if (workResult == Result::SUCCESS)
    {

    }

    Another anotherRes = Another::WORK;
    //if (workResult == anotherRes)
    {

    }


    // СТРУКТУРЫ
    Student* ptr = new Student{ 10, 160, "Paul" };
    ptr->GetInfo();
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
